﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2_SE
{
    public class MyStack<T> : IATD<T>
        where T : IComparable<T>
    {
        private MyLinkedList<T> list = new MyLinkedList<T>();
        private MyLinkedList<T>.Node<T> t = new MyLinkedList<T>.Node<T>();
        T IATD<T>.Pop()
        { 
            list.RemoveLast();
            if(list._lastElement ==  null)
              throw new InvalidOperationException("Удален последний элемент стека");
            return list._lastElement.Data;
        }

        void IATD<T>.Push(T data)
        {
            list.Add(data);
        }

        T IATD<T>.LastElement()
        {
            T last = list.GetLast();
            return last;
        }

       int IATD<T>.CountE()
        {
            int count = list.Count();
            return count;
        }

        public void PrintToConsole()
        {
            list.Print();
        }

        T IATD<T>.this[int index]
        {
            get
            {
                if (index < 0)
                    throw new ArgumentOutOfRangeException();
                MyLinkedList<T>.Node<T> current = list._firstElement;

                for (int i = 0; i < index; i++)
                {
                    if (current.Next == null)
                        throw new ArgumentOutOfRangeException();
                    
                    current = current.Next;
                }
                return current.Data;
            }

        }

      
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < list.Count(); i++)
                yield return list[i];
        }

        
   
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public object Clone()
        {
            var myStack = new MyStack<T>();

            for (int i = 0; i < list.Count(); i++)
                myStack.list.Add(list[i]);

            return myStack;
        }


        public int CompareTo(IATD<T> o)
        {
            MyStack<T> other = (MyStack<T>)o;
            for (int i = 0; i < list.Count(); i++)
            {
                int compRes = list[i].CompareTo(other.list[i]);

                if (compRes != 0)
                    return compRes;
            }

            return 0;
        }
    }
}
