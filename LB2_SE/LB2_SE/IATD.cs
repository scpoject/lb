﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2_SE
{
    public interface IATD<T> : IEnumerable<T>, IComparable<IATD<T>>, ICloneable
    {
      T Pop();
      void Push(T data);
      T LastElement();

      T this[int index]
      {
          get;
      }
      int CountE();

    }
}
