﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace LB2_SE
{
    class DArray_Steck<T>: IATD<T>
            where T : IComparable<T>
    {
        private T[] _array;
        private const int defaultCapacity = 10;
        private int size;

        public DArray_Steck()
        {
            this.size = 0;
            this._array = new T[defaultCapacity];
        }

        public bool isEmpty()
        {
            return this.size == 0;
        }

        int IATD<T>.CountE()
        {
            int count = _array.Count();
            return count;
        }

        T IATD<T>.Pop()
        {
            if (this.size == 0)
            {
                throw new InvalidOperationException();
            }
            return this._array[--this.size];
        }

        void IATD<T>.Push(T data)
        {
            if (this.size == this._array.Length)
            {
                T[] newArray = new T[2 * this._array.Length];
                Array.Copy(this._array, 0, newArray, 0, this.size);
                this._array = newArray;
            }
            this._array[this.size++] = data;
        }

        T IATD<T>.LastElement()
        {
            if (this.size == 0)
            {
                throw new InvalidOperationException();
            }
            return this._array[this.size - 1];
        }
        public T this[int index]
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < _array.Count(); i++)
                yield return _array[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object Clone()
        {
            var myStack = new DArray_Steck<T>();

            return myStack;
        }

        public int CompareTo(IATD<T> o)
        {
       DArray_Steck<T> other = (DArray_Steck<T>)o;
            for (int i = 0; i < _array.Count(); i++)
            {
                int compRes = _array[i].CompareTo(other._array[i]);

                if (compRes != 0)
                    return compRes;
            }

            return 0;
        }

    }
}
