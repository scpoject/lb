﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace LB2_SE
{
    public class SQueue<T> : IATD<T>
        where T : IComparable<T>
    {
        private SList<T> list = new SList<T>(); //cказал, что не нужно
        private SList<T>.Node<T> t = new SList<T>.Node<T>();
         T IATD<T>.Pop()
        {
            list.RemoveFirst();
         

            return list._first.Data;
        }

        void IATD<T>.Push(T data)
        {
            list.Add(data);
        }

         T IATD<T>.LastElement()
        {
            T last = list.GetLast();
            return last;
        }

         int IATD<T>.CountE()
        {
            int count = list.Count();
            return count;
        }

         T IATD<T>.this[int index]
        {

            get
            {
                if (index < 0)
                    throw new ArgumentOutOfRangeException();
                SList<T>.Node<T> current = list._first;

                for (int i = 0; i < index; i++)
                {
                    if (current.Next == null)
                        throw new ArgumentOutOfRangeException();

                    current = current.Next;
                }
                return current.Data;
            }

        }
         public IEnumerator<T> GetEnumerator()
         {
             for (int i = 0; i < list.Count(); i++)
                 yield return list[i];// формирует значение, выдаваемое на очередной итерации
         }

         IEnumerator IEnumerable.GetEnumerator()
         {
             return GetEnumerator();//возвращает объект типа IEnumerator (перечислитель)
         }

         public object Clone()
         {
             var queue = new SQueue<T>();

             for (int i = 0; i < list.Count(); i++)
                 queue.list.Add(list[i]);//сказал, что list не должен торчать на ружу, нужно обращаться к push вместо list.add,тк нужно пользоваться интерфейсом. но зачел, тк работает, но задавал много вопросов

             return queue;
         }
         public int CompareTo(IATD<T> o)
         {
             SQueue<T> queue2 = (SQueue<T>)o;
             for (int i = 0; i < list.Count(); i++)
             {
                 int compRes = list[i].CompareTo(queue2.list[i]);

                 if (compRes != 0)
                     return compRes;
             }

             return 0;
         }


    }

}
