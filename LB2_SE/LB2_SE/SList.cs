﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2_SE
{
    public class SList<T>
    {
        public class Node<T>
        {
            public Node<T> Next;
            public Node<T> Prev;

            public T Data;
        }

        public Node<T> _first;
        public Node<T> _last;



        public bool IsEmpty
        {
            get { return _first == null; }
        }
        public void Add(T element)
        {
            Node<T> newNode = new Node<T> { Data = element };

            if (IsEmpty)
            {
                _first = newNode;
                _last = newNode;//для работы теста LastElementTest. вытащить последний элемент из стека с одним(первым элементом, который является и последним)
            }
            else
            {
                Node<T> n = _first;

                while (n.Next != null)
                    n = n.Next;

                n.Next = newNode;
                newNode.Prev = n;

                _last = newNode;
            }
        }

        public void RemoveFirst()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Очередь пуста");

            else
            {
                if (_first != null)
                {
                    _first = _first.Next;

                    _first.Prev = null;
                }
                else _first = null;
            }
        }

        public T this[int index]
        {
            get
            {
                if (index < 0)
                    throw new ArgumentOutOfRangeException();
                Node<T> current = _first;

                for (int i = 0; i < index; i++)
                {
                    if (current.Next == null)
                        throw new ArgumentOutOfRangeException();

                    current = current.Next;
                }
                return current.Data;
            }

        }


        public T GetLast()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Список пустой");

            return _last.Data;
        }

        public int Count()
        {

            if (IsEmpty)
                throw new InvalidOperationException("Список пустой");
            else
            {
                Node<T> n = _first;
                int count = 0;

                while (n != null)
                {
                    count++;
                    n = n.Next;
                }

                return count;

            }
        }
    }


}
