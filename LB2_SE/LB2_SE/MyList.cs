﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2_SE
{
    public class MyLinkedList<T>
    {
      
      
        public class Node<T> 
        {
            public Node<T> Next;
            public Node<T> Prev;

            public T Data;
        }

        public Node<T> _firstElement;
        public Node<T> _lastElement;

        

        public bool IsEmpty
        {
            get { return _firstElement == null; }
        }
        public void Add(T element) 
        {
            Node<T> newNode = new Node<T> { Data = element };

            if (IsEmpty)
            {
                _firstElement = newNode;
                _lastElement = newNode;
            }
              
            else
            {
                Node<T> n = _firstElement;

                while (n.Next != null)
                    n = n.Next;

                n.Next = newNode;
                newNode.Prev = n;

                _lastElement = n.Next;   
            }
        }

        public void RemoveLast()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Список пустой");
            else
                {
                    if (_lastElement != null)
                    {
                       _lastElement = _lastElement.Prev;

                       _lastElement.Next = null;
                    }
                   
                    else
                    _firstElement = null;
                }
            
        }

        public T GetLast()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Список пустой");

            return _lastElement.Data;
        }
        

        public void Print()
        {
            if (IsEmpty)
                throw new InvalidOperationException("Список пустой");
            else
            {
                Node<T> n = _firstElement;

                while (n!= null)
                {
                    Console.WriteLine(n.Data);
                    n = n.Next;
                }
            }
        }




        public int Count()
        {

            if (IsEmpty)
                    throw new InvalidOperationException("Список пустой");
            else
            {
                Node<T> n = _firstElement;
                int count = 0;

                while (n!= null)
                {
                    count++;
                    n = n.Next;
                }

                return count;
                  
            }
        }

        public T this[int index]
        {
            get
            {
                if (index < 0)
                    throw new ArgumentOutOfRangeException();
                Node<T> current = _firstElement;

                for (int i = 0; i < index; i++)
                {
                    if (current.Next == null)
                        throw new ArgumentOutOfRangeException();

                    current = current.Next;
                }
                return current.Data;
            }

        }
                 
            
        }
    }

