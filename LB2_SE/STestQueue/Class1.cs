﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LB2_SE;
namespace STestQueue
{
    [TestFixture]
    public class STest
    {
        [Test()]
        public void PushTest()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(2);
            queue.Push(3);
            string outs = queue[0].ToString() + queue[1].ToString() + queue[2].ToString();
            Assert.AreEqual("123", outs);
        }

        [Test()]
        public void PopTest()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(2);
            queue.Push(3);
            int n1 = queue.Pop();
            int n2 = queue.Pop();
            Assert.AreEqual(2, n1);
            Assert.AreEqual(3, n2);
        }
        [Test()]
        public void PopTest2()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(2);
            queue.Push(3);
            queue.Pop();

            string outs = queue[0].ToString() + queue[1].ToString();
            Assert.AreEqual("23", outs);
        }

        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void PopException()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Pop();
        }
       
        [Test()]
        public void CountTest()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(3);
            queue.Push(5);
            int n = queue.CountE();
            Assert.AreEqual(3, n);
        }
        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CountException()
        {
            IATD<int> queue = new SQueue<int>();
            queue.CountE();
        }
      
        [Test()]
        public void LastElementTest() //добавить этот тест(пуш 1 элемент в очередь и вытащить последний
        {
            IATD<int> queue = new SQueue<int>();
           
            queue.Push(5);

            Assert.AreEqual(queue.LastElement(), 5);
        }
        [Test()]
        public void LastElementTest3()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(2);
            queue.Push(5);

            Assert.AreEqual(queue.LastElement(), 5);
        }

        [Test()]
        public void LastElementTest2()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(-2);
            queue.Push(3);
            queue.Pop();
            queue.Pop();

            Assert.AreEqual(queue.LastElement(), 3);
        }

        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void GetLastException()
        {
            IATD<int> queue = new SQueue<int>();
            queue.LastElement();
        }
        [Test()]
        public void IndexTest()
        {
            IATD<int> queue= new SQueue<int>();
            queue.Push(1);
            queue.Push(-2);
            queue.Push(3);

            Assert.AreEqual(3, queue[2]);
        }
        
        [Test()]
        public void IndexTest2()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            queue.Push(-2);
            queue.Push(3);

            Assert.AreEqual(1, queue[0]);
        }
        
        [Test()]
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void IndexException()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            int i = queue[1];

        }
        [Test()]
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void IndexException2()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(1);
            int i = queue[-1];

        }

      
        [Test()]
        public void CompareToTest()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(1);
            queue2.Push(10);
            queue2.Push(-5);
            queue2.Push(2);

            Assert.AreEqual(queue.CompareTo(queue2), 0);
        }
        [Test()]
        public void CompareToTest2()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(2   );
            queue2.Push(11);
            queue2.Push(-4);
            queue2.Push(3);

            Assert.AreEqual(queue.CompareTo(queue2), -1);
        }
        [Test()]
        public void CompareToTest3()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(1);
            queue2.Push(11);
            queue2.Push(-5);
            queue2.Push(2);

            Assert.AreEqual(queue.CompareTo(queue2), -1);
        }
        [Test()]
        public void CompareToTest4()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(0);
            queue2.Push(9);
            queue2.Push(-6);
            queue2.Push(1);

            Assert.AreEqual(queue.CompareTo(queue2), 1);
        }
        public void CompareToTest5()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(1);
            queue2.Push(10);
            queue2.Push(-6);
            queue2.Push(2);

            Assert.AreEqual(queue.CompareTo(queue2), 1);
        }

        [Test()]
        public void CloneTest()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = (SQueue<int>)queue.Clone();
            Assert.AreEqual(queue.CompareTo(queue2), 0);
        }



        [Test()]
        public void EnumeratorTest()
        {
            IATD<int> queue = new SQueue<int>();
            queue.Push(10);
            queue.Push(20);
            queue.Push(0);
            queue.Push(13);
            queue.Push(-5);

            int i = 0;
            foreach (var element in queue)
            {
                i++;
            }
            Assert.AreEqual(i, 5);
        }


        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CloneException()
        {
            IATD<int> queue = new SQueue<int>();
            IATD<int> queue2 = (SQueue<int>)queue.Clone();
        }

        //сомнения
        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CompareToExaption()
        {
            IATD<int> queue = new SQueue<int>();
            IATD<int> queue2 = new SQueue<int>();
            queue.CompareTo(queue2);
        }
        
//сомнения
        [Test()]
        public void CopmareToTest5()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(2);

            Assert.AreEqual(queue.CompareTo(queue2), -1);
        }
        [Test()]
        public void CopmareToTest6()
        {
            IATD<int> queue = new SQueue<int>();

            queue.Push(1);
            queue.Push(10);
            queue.Push(-5);
            queue.Push(2);

            IATD<int> queue2 = new SQueue<int>();

            queue2.Push(0);
           
            Assert.AreEqual(queue.CompareTo(queue2), 1);
        }
    
}
}
