﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LB2_SE;
namespace LB2_SE_Tests
{
    [TestFixture]
    public class IATDTest
    {
        
        protected IATD<int> myStack;

        [Test()]
        public void PushTest()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(-9);
            myStack.Push(0);
            myStack.Push(14);
            myStack.Push(2);
            myStack.Push(75);

            int actual = myStack[1] + myStack[3] + myStack[0] + myStack[2] + myStack[4];
            int expected = 82;
            Assert.AreEqual(expected, actual);
        }



        [Test()]
        public void PopTest()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);



            int actual = myStack.Pop();
            int expected = -2;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void CountETest()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);



            int actual = myStack.CountE();
            int expected = 5;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void LastElementTest()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);



            int actual = myStack.LastElement();
            int expected = 5;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void LastElementTest3()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(11);
           
            int actual = myStack.LastElement();
            int expected = 11;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void EnumeratorTest()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);

            int i = 0;
            foreach (var element in myStack)
            {
                i++;
            }



            int actual = i;
            int expected = 5;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void CompareToTest()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);

            IATD<int> otherStack = new MyStack<int>();

            otherStack.Push(11);
            otherStack.Push(0);
            otherStack.Push(4);
            otherStack.Push(-2);
            otherStack.Push(5);


            int actual = myStack.CompareTo(otherStack);
            int expected = 0;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void CompareToTest2()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);

            IATD<int> other = new MyStack<int>();

            other.Push(11);
            other.Push(0);
            other.Push(-4);
            other.Push(-2);
            other.Push(5);


            int actual = myStack.CompareTo(other);
            int expected = 1;
            Assert.AreEqual(expected, actual);
        }


        [Test()]
        public void CompareToTest3()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(-4);
            myStack.Push(-2);
            myStack.Push(5);

            IATD<int> other = new MyStack<int>();

            other.Push(11);
            other.Push(0);
            other.Push(4);
            other.Push(-2);
            other.Push(5);



            int actual = myStack.CompareTo(other);
            int expected = -1;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CompareToTest4()
        {
            IATD<int> myStack = new MyStack<int>();
            IATD<int> other = new MyStack<int>();

            myStack.CompareTo(other);
            
        }

        [Test()]
        public void CloneTest()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.Push(11);
            myStack.Push(0);
            myStack.Push(4);
            myStack.Push(-2);
            myStack.Push(5);

            IATD<int> other = new MyStack<int>();
            other = (IATD<int>)myStack.Clone();

            int actual = myStack.CompareTo(other);
            int expected = 0;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CloneTest2()
        {
            IATD<int> myStack = new MyStack<int>();
            IATD<int> other = new MyStack<int>();

            other = (IATD<int>)myStack.Clone();
        }



        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void PopException()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.Pop();
        }



        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CloneTest3()
        {
            IATD<int> myStack = new MyStack<int>();
            IATD<int> otherStack = new MyStack<int>();
            otherStack = (MyStack<int>)myStack.Clone();

        }

        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void CountException()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.CountE();

        }


        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void LastElementTest2()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.LastElement();

        }

       

        [Test()]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void PopExceptionTest()
        {
            IATD<int> myStack = new MyStack<int>();

            myStack.Pop();
           
            
        }

        [Test()]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void PopExceptionTest2()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(0);
            myStack.Pop();
        }

        [Test()]
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void IteratorExceptionTest()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(75);
            int i = myStack[-1];
        }

        [Test()]
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void IteratorExceptionTest2()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(0);
            int i = myStack[1];
        }

        [Test()]
        public void IteratorExceptionTest3()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(11);
            myStack.Push(12);
            myStack.Push(1789);

            int actual = myStack[0];
            int expected = 11;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void IteratorExceptionTest4()
        {
            IATD<int> myStack = new MyStack<int>();
            myStack.Push(117);
            myStack.Push(11);

            int actual = myStack[1];
            int expected = 11;
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void CompareToTest5()
        {
            IATD<int> myStack = new MyStack<int>();

     
            myStack.Push(0);
            myStack.Push(-4);
            myStack.Push(-2);
            myStack.Push(5);

            IATD<int> other = new MyStack<int>();

            other.Push(11);
            other.Push(0);
            other.Push(4);
            other.Push(-2);
            other.Push(5);



            int actual = myStack.CompareTo(other);
            int expected = -1;
            Assert.AreEqual(expected, actual);
        }


        [Test()]
        public void CompareToTest6()
        {
            IATD<int> myStack = new MyStack<int>();


            myStack.Push(0);
            myStack.Push(-4);
            myStack.Push(-2);
            myStack.Push(5);

            IATD<int> other = new MyStack<int>();

            other.Push(4);
            other.Push(-2);
            other.Push(5);



            int actual = other.CompareTo(myStack);
            int expected = 1;
            Assert.AreEqual(expected, actual);
        }

    }
}
