﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LB2_SE;


namespace Test_Array
{
    [TestFixture]
    class STest
    {
        [Test()]

        public void Tests_1()
        {
            LB2_SE.SList<int> myList = new SList<int>();
            myList.Add(1);
            string outs = myList[0].ToString();
            Assert.AreEqual("1", outs);
        }
    }
}
