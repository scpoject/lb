﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{ // отвечает за представление чека, учет его содержимого
     public class Bill
    {
        private List<Item> _items; 
        private Customer _customer;
        private double thisAmount = 0;
        private double discount = 0;
        private int bonus = 0;
        private double totalAmount = 0;
        private int totalBonus = 0;
        private Item each;
        String result;
        public Bill(Customer customer) 
        { 
            this._customer = customer; 
            this._items = new List<Item>(); 
        } 
  
        public void addGoods(Item arg) 
        { 
            _items.Add(arg); 
        }

        public void GetHeader() 
        {
            result = "Счет для " + _customer.getName() + "\n"
                     +"\t" + "Название" + "\t" + "Цена" +
                      "\t" + "Кол-во" + "Стоимость" + "\t" + "Скидка" +
                      "\t" + "Сумма" + "\t" + "Бонус" + "\n";   
        }


        public void GetItemString() 
        {
           result += "\t" + each.getGoods().getTitle() 
                                       + "\t" 
                                       + "\t" + each.getPrice() 
                                       + "\t" + each.getQuantity() + 
                                         "\t" + (each.getQuantity() * each.getPrice()).ToString() 
                                       + "\t" + discount.ToString() + "\t" + thisAmount.ToString() 
                                       + "\t" + bonus.ToString() + "\n";
        }

         
        public void GetFooter() 
        {
            result += "Сумма счета составляет " 
                                            + totalAmount.ToString() 
                                            + "\n"
                                            + "Вы заработали " 
                                            + totalBonus.ToString() + " бонусных балов";
        }


        public void GetBonus()
        {
            switch (each.getGoods().getPriceCode())
            {
                case Goods.REGULAR:             
                    bonus = (int)(each.getQuantity() * each.getPrice() * 0.05);
                    break;           
                case Goods.SALE:                     
                    bonus = (int)(each.getQuantity() * each.getPrice() * 0.01);
                    break;
            }
        }

        public void GetDiscount()
        {
            switch (each.getGoods().getPriceCode())
            {
                case Goods.REGULAR:
                    if (each.getQuantity() > 2)
                        discount = (each.getQuantity() * each.getPrice()) * 0.03; // 3%
                    if (each.getQuantity() > 5)
                        discount += _customer.useBonus((int)(each.getQuantity() * each.getPrice()));
                    break;
                case Goods.SPECIAL_OFFER:
                    if (each.getQuantity() > 10)
                        discount = (each.getQuantity() * each.getPrice()) * 0.005; // 0.5%
                    if (each.getQuantity() > 1)
                        discount = _customer.useBonus((int)(each.getQuantity() * each.getPrice()));

                    break;
                case Goods.SALE:
                    if (each.getQuantity() > 3)
                        discount = (each.getQuantity() * each.getPrice()) * 0.01; // 0.1%

                   
                    break;
            }
        }
        public String statement() 
        { 
            totalAmount = 0; 
            totalBonus = 0; 
            List<Item>.Enumerator items = _items.GetEnumerator();
            GetHeader();
            while (items.MoveNext()) 
            {
                //удалил thisAmount
                    discount = 0;
                    bonus = 0;
                    each = (Item)items.Current;
                   
                    //определить сумму для каждой строки 
               
                    GetBonus();
                    GetDiscount();
                    // сумма 
                    thisAmount = each.getQuantity() * each.getPrice();

                    // используем бонусы 
                     // учитываем скидку 
                        thisAmount = each.getQuantity() * each.getPrice() - discount;
                        GetItemString(); 
                        totalAmount += thisAmount; 
                        totalBonus += bonus; 
                }
                        GetFooter();
                        //Запомнить бонус клиента 
                        _customer.receiveBonus(totalBonus);

                        
                        
                       

                        return result;
            }
    } 
}







